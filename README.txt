AVR firmware updater tool for bootloader v2, v3, v4.

The destination board have to be powered up an running for the updater tool to auto-detect the board.
The firmware on the destination board must be set to 115200 bps.

The parameter "--flashsize=" should be set to the size of the internal flash of the controller
	- For Attiny1606: "--flashsize=16384"
	- For Attiny3224: "--flashsize=32768"

Requirements:
	The uploader tool needs the python3 packages "pyserial" and "intelhex" installed.
	You can install them from your distribution packages or by pip.

Command syntax:
	python3 AvrUploader.py --file=[Path to HEX file] --comport=[serial device] --flashsize=[flash size]

Example for Linux with Attiny1606:
	python3 AvrUploader.py --file=/mnt/ramdisk/testfile.hex --comport=/dev/ttyUSB0 --flashsize=16384
