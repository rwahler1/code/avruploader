#!/usr/bin/python3
# -*- coding: utf-8 -*-

from builtins import bytes, int
import argparse
import os
import time
from datetime import datetime
from serial import Serial, SerialException
from intelhex import IntelHex, IntelHexError

FIRMWARE_RESPONSE_WAIT = b'$'
FIRMWARE_CMD_VERSION = b'version\r\n'
FIRMWARE_CMD_BOOTLOADER = b'boot\r\n'
FIRMWARE_CMD_UPDATE_BOOTLOADER = b'update_bl\r\n'
FIRMWARE_RESPONSE_VERSION = b'Version:'
BOOTLOADER_CMD_FLASH = b'f'
BOOTLOADER_CMD_VERSION = b'v'
BOOTLOADER_CMD_BOOTFLAG = b'+'
BOOTLOADER_CMD_START = b's'
BOOTLOADER_CMD_INTERRUPT = b'b'
BOOTLOADER_RESPONSE_WAIT = b'?'
BOOTLOADER_RESPONSE_CMD = b'Cmd:\n'
BOOTLOADER_RESPONSE_OK = b'OK\n'
# V2 = 'start\n', V3 = 'OK\n'
BOOTLOADER_RESPONSE_START = [b'Start\n', b'OK\n']
BOOTLOADER_SUPPORTED_VERSION = [2, 3, 4]

UPLOADER_VERSION = 'v1.4'

UART_TIMEOUT_DEFAULT = 0.5
UART_TIMEOUT_BOOTLOADER = 0.15
UART_TIMEOUT_FIRMWARE = 1.2

global stepCounter
global bootloaderVersion
stepCounter = 0
bootloaderVersion = 0

def processFirmware(file, flashsize, bootend):
	# Load HEX file with firmware and convert to byte array
	# Check if start address in HEX file starts withBOOTEND * 256
	# Pad byte array with 0xFF to write flash with 64 byte blocks
	ih = IntelHex()
	fileextension = file[-3:]
	try:
		ih.loadfile(file, format=fileextension)
	except IntelHexError:
		print('Error: Hex file could not be loaded')
		exit(1)

	if len(ih.segments()) == 1:
		append = ih.maxaddr()
	else:
		# Falls Configuration Bits gesetzt werden, dann gibt es ein zweites Segment mit sehr hohen Adressen
		append = ih.segments()[0][1] - 1
	if append is None or append > flashsize:
		print(f'Error: Flash size given: {flashsize}, but top address: {append}')
		exit(1)

	appstart = ih.minaddr()
	if appstart is None or appstart != 256 * bootend:
		print('Error: Invalid hex start address')
		exit(1)

	appsize = append - appstart + 1
	if appsize > (flashsize - 256 * bootend):
		print (f'Error: Flash size given: {flashsize}, but required: {appsize}')
		exit(1)

	fill = 64 - appsize % 64
	appsize += fill

	app = ih.tobinarray(end=appsize-1 + appstart)

	if appsize != len(app):
		print('Error: Firmware size compare failed')
		exit(1)

	print('The controller will be programmed with the following data')
	print(f'File: {file}')
	print(f'Address: 0x{appstart:04X} - 0x{append:04X}')
	print(f'Firmware size: {appsize - fill} bytes (without padding)')
	print(f'Firmware size: {appsize} bytes (with padding)\n')
	print('For programming the firmware needs to be set to 115200 Bps!')

	start = input('Start programming? (y/n)? ')
	if start != 'y':
		exit(0)
	return app, appsize, appstart

def initUart(comport, baudrate):
	global stepCounter

	# Init UART
	uart = None
	stepCounter += 1
	print(f'{stepCounter}. Opening UART interface {comport}: ', end='')
	for _ in range(10):
		try:
			uart = Serial(comport, baudrate=baudrate, timeout=UART_TIMEOUT_DEFAULT)
			break
		except SerialException:
			print('.', end='')
			time.sleep(0.5)

	if uart is None:
		print(f'UART interface {comport} could not be opened')
		exit(1)

	print('OK')
	return uart

def skipFirmwareWaitLoop(uart):
	uart.write(b'\r\n')

	# Catch start output and ignore
	uart.timeout = UART_TIMEOUT_FIRMWARE
	uart.readlines()

def checkBootloaderVersion(uart):
	global stepCounter
	global bootloaderVersion

	stepCounter += 1
	print(f'{stepCounter}. Check bootloader version: ', end='')
	uart.write(BOOTLOADER_CMD_VERSION)
	result = uart.readlines()
	if len(result[0]) != 2 or int(result[0].strip().decode("utf-8")) not in BOOTLOADER_SUPPORTED_VERSION:
		print(f'Unsupported bootloader version: {result}')
		exit(1)
	bootloaderVersion = int(result[0].strip().decode("utf-8"))
	print(f'OK (v{bootloaderVersion})')

def prepareBootloaderUpdate(uart):
	global stepCounter

	# Lower serial timeout
	uart.timeout = UART_TIMEOUT_BOOTLOADER

	stepCounter += 1
	print(f'{stepCounter}. Detect current state of firmware: ', end='')

	# Check if the controler outputs something right now
	for _ in range(20):
		rxData = uart.read(1)
		if rxData == BOOTLOADER_RESPONSE_WAIT:
			# Bootloader >= V3 is running for 3s after reset before starting the firmware
			for _ in range(30):
				rxData = uart.read(1)
				if rxData == 'O' or rxData == 'K' or rxData == '\n' :
					break
		elif rxData == FIRMWARE_RESPONSE_WAIT:
			# Firmware is running directly after the bootloader and is in waittime loop for max 30s
			skipFirmwareWaitLoop(uart)
			break

	print('OK')

	# Set timeout to default
	uart.timeout = UART_TIMEOUT_DEFAULT

	# Send dummy data to set receiver into defined state
	uart.readlines()
	uart.write(b'.\r\n')
	uart.readlines()

	stepCounter += 1
	print(f'{stepCounter}. Check if firmware is running: ', end='')
	uart.write(FIRMWARE_CMD_VERSION)
	result = uart.readlines()
	if len(result) == 0 or not result[0].startswith(FIRMWARE_RESPONSE_VERSION):
		# 2. Try
		time.sleep(0.5)
		uart.write(FIRMWARE_CMD_VERSION)
		result = uart.readlines()
		if len(result) == 0 or not result[0].startswith(FIRMWARE_RESPONSE_VERSION):
			print(f'Wrong answer from firmware: {result}')
			exit(1)
	version = result[0].strip().decode("utf-8")[9:]
	print(f'OK (v{version})')

	# Bootloader Modus aktivieren
	print(f'{stepCounter}. Start bootloader update mode of firmware: ', end='')
	uart.write(FIRMWARE_CMD_UPDATE_BOOTLOADER)
	result = uart.readlines()
	if len(result) == 0 or result[0] != BOOTLOADER_RESPONSE_OK:
		print(f'Wrong answer from firmware: {result}')
		exit(1)

	checkBootloaderVersion(uart)

def startBootloader(uart):
	global stepCounter

	# Lower serial timeout
	uart.timeout = UART_TIMEOUT_BOOTLOADER

	bootloaderStart = False
	firmwareStart = False

	stepCounter += 1
	print(f'{stepCounter}. Detect current state of firmware: ', end='')

	# Check if the controler outputs something right now
	for _ in range(10):
		rxData = uart.read(1)
		if rxData == BOOTLOADER_RESPONSE_WAIT:
			# Bootloader >= V3 is running for 3s after reset before starting the firmware
			bootloaderStart = True
			break
		elif rxData == FIRMWARE_RESPONSE_WAIT:
			# Firmware is running directly after the bootloader and is in waittime loop for max 30s
			firmwareStart = True
			break

	print('OK')

	# Set timeout to default
	uart.timeout = UART_TIMEOUT_DEFAULT

	if bootloaderStart is True:
		# Only bootloader >= V3
		stepCounter += 1
		print(f'{stepCounter}. Try to access bootloader: ', end='')
		# Abort bootloader wait time
		uart.write(BOOTLOADER_CMD_INTERRUPT)
		result = uart.readlines()
		if len(result) == 0 or result[-1] != BOOTLOADER_RESPONSE_CMD:
			print(f'Wrong answer to start bootloader: {result}')
			exit(1)
		print('OK')
	else:
		if firmwareStart is True:
			# Skip firmware start sequence
			skipFirmwareWaitLoop(uart)

		# Send dummy data to set receiver into defined state
		uart.readlines()
		uart.write(b'.\r\n')
		uart.readlines()

		stepCounter += 1
		print(f'{stepCounter}. Check if firmware is running: ', end='')
		uart.write(FIRMWARE_CMD_VERSION)
		result = uart.readlines()
		if len(result) == 0 or not result[0].startswith(FIRMWARE_RESPONSE_VERSION):
			# 2. Try
			time.sleep(0.5)
			uart.write(FIRMWARE_CMD_VERSION)
			result = uart.readlines()
			if len(result) == 0 or not result[0].startswith(FIRMWARE_RESPONSE_VERSION):
				print(f'Wrong answer from firmware: {result}')
				exit(1)
		version = result[0].strip().decode("utf-8")[9:]
		print(f'OK (v{version})')

		stepCounter += 1
		print(f'{stepCounter}. Start bootloader from firmware: ', end='')
		uart.write(FIRMWARE_CMD_BOOTLOADER)
		result = uart.readlines()
		if len(result) == 0 or result[-1] != BOOTLOADER_RESPONSE_CMD:
			print(f'Wrong answer to start bootloader: {result}')
			exit(1)
		print('OK')

	checkBootloaderVersion(uart)

def flashFirmware(uart, app, appsize, appstart):
	global stepCounter

	stepCounter += 1
	if appstart == 0:
		# Bootloader aktualisieren
		pass
	else:
		# Firmware aktualisieren
		print(f'{stepCounter}. Start flash mode of bootloader: ', end='')
		uart.write(BOOTLOADER_CMD_FLASH)
		result = uart.readlines()
		if result[0] != BOOTLOADER_RESPONSE_OK:
			print(f'Wrong answer from bootloader: {result}')
			exit(1)
	print('OK')

	# Send size of area to be flashed
	# Example: 0x1234 Bytes
	stepCounter += 1
	print(f'{stepCounter}. Send file size to bootloader ({appsize} Bytes): ', end='')
	uart.write(appsize.to_bytes(2, 'big'))
	result = uart.readlines()
	resultOk = False
	if len(result) != 0:
		if bootloaderVersion < 4:
			# Als Antwork wird "OK\n" erwartet
			resultOk = result[0] == BOOTLOADER_RESPONSE_OK
		else:
			# Als Antwort wird die Dateigröße zurück erwartet
			rxAppsize = int.from_bytes(result[0].encode(encoding="utf-8"), byteorder='big')
			if appsize == rxAppsize:
				resultOk = True

	if resultOk is False:
		print(f'Wrong answer from bootloader: {result}')
		exit(1)

	print('OK')

	# Send data
	startTime = datetime.now()
	stepCounter += 1
	counter = 0
	byte = []
	tmp_byte = []
	for byte in app:
		byte = bytes([byte])
		# Send byte to controller
		uart.write(byte)
		# Wait for echo of sended byte
		tmp_byte = uart.read(1)
		# Check if echo is identical
		if tmp_byte != byte:
			print(f'\nError programming address {counter + appstart}')
			tmp_byte = int.from_bytes(tmp_byte, byteorder='big')
			byte = int.from_bytes(byte, byteorder='big')
			print(f'Received 0x{tmp_byte:02x} but 0x{byte:02x} expected')
			exit(1)

		counter += 1
		print(f'\r{stepCounter}. Upload firmware: {counter:5}/{appsize} Bytes', end='')

	timeDiff = (datetime.now() - startTime).total_seconds()
	transferRate = len(app) / timeDiff

	print('\n')

	result = uart.readlines()
	if len(result) == 0 or result[0] != BOOTLOADER_RESPONSE_OK:
		print(f'Wrong answer after flashing: {result}')
		exit(1)

	stepCounter += 1
	if appstart == 0:
		# Bootloader
		print(f'{stepCounter}. New bootloader installed (duration: {timeDiff:.1f}s, speed: {transferRate:.0f} bytes/s)')
	else:
		# Firmware
		print(f'{stepCounter}. New firmware installed (duration: {timeDiff:.1f}s, speed: {transferRate:.0f} bytes/s)')
		print(f'{stepCounter}. Set boot flag for new firmware: ', end='')
		uart.write(BOOTLOADER_CMD_BOOTFLAG)
		result = uart.readlines()
		if len(result) == 0 or result[0] != BOOTLOADER_RESPONSE_OK:
			print(f'Wrong answer from bootloader: {result}')
			exit(1)
		print('OK')

		stepCounter += 1
		print(f'{stepCounter}. Start new firmware: ', end='')
		uart.write(BOOTLOADER_CMD_START)
		result = uart.readlines()
		if len(result) == 0 or result[0] not in BOOTLOADER_RESPONSE_START:
			print(f'Wrong answer from bootloader: {result}')
			exit(1)
		print('OK')

def checkFirmwareRunning(uart):
	global stepCounter

	stepCounter += 1
	print(f'{stepCounter}. Check if new firmware is running: ', end='')

	# Skip firmware wait time
	time.sleep(0.5)
	result = uart.write(b'\r\n')

	# Catch start output and ignore
	uart.timeout = UART_TIMEOUT_FIRMWARE
	uart.readlines()

	# Set serial timeout to default
	uart.timeout = UART_TIMEOUT_DEFAULT

	uart.write(FIRMWARE_CMD_VERSION)
	result = uart.readlines()
	if len(result) == 0 or not result[0].startswith(FIRMWARE_RESPONSE_VERSION):
		print(f'Wrong answer from firmware: {result}')
		exit(1)
	print('OK')

	stepCounter += 1
	print(f'{stepCounter}. Response: {result[0].strip().decode("utf-8")}')
	print('Successfully programmed new firmware into controller')

def main():
	parser = argparse.ArgumentParser()
	parser.add_argument('--file', required=True, help='Hex file for programming into the controller')
	parser.add_argument('--comport', required=True, help='UART interface')
	parser.add_argument('--flashsize', type=int, default=32768, help='Total flash size of the controller (default=32768)')
	parser.add_argument('--bootend', type=int, default=2, help='Flash offset blocks (in size of 256 bytes) for the bootloader (default=2)')
	parser.add_argument('--baudrate', type=int, default=115200, help='UART speed in Bps (default=115200)')
	args = parser.parse_args()

	file = args.file
	comport = args.comport
	flashsize = args.flashsize
	bootend = args.bootend
	baudrate = args.baudrate

	print(f'AVR Uploader {UPLOADER_VERSION} by Rainer Wahler (RWahler@gmx.net)\n')

	if not os.path.exists(file):
		print(f'Hex file {file} doesn\'t exist')
		exit(1)

	# Load HEX file and convert it
	app, appsize, appstart = processFirmware(file, flashsize, bootend)

	# Init UART
	uart = initUart(comport, baudrate)

	if appstart == 0:
		# Bootloader soll aktualisiert werden
		prepareBootloaderUpdate(uart)
	else:
		# Firmware soll aktualisiert werden
		startBootloader(uart)

	# Update firmware/bootloader
	flashFirmware(uart, app, appsize, appstart)

	if appstart != 0:
		# Check new Firmware
		checkFirmwareRunning(uart)

main()
